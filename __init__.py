# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .type_activity import TypeActivity
from .location import MaintenanceLocation
from .equipment import (Equipment, MaintenanceEquipmentPart, CheckListEquipment,
    EquipmentOperationCard, EquipmentKind, VehiclePoolReport, EventEquipment,
    VehicleTechMechanical, EquipmentCategory, Brand, VehiclePoolStart,
    VehiclePool, MaintenanceEquipmentProduct, EquipmentInsurance, Classification)
from .request_service import (RequestService, RequestServiceReport, CheckListService,
    InvoiceLineRequestService)
from .planning import (Period, Planning, PlanningLine, FrecuencyKind, PlanningCode,
    PlanningCodeFrecuencyKind, LineFrecuency, CreateForecast, CreateForecastStart,
    AddLastMaintenanceDate, AddLastMaintenanceDateStart)
from .schedule import (Schedule, CreateScheduling, ScheduleForecast,
    ScheduleForecastStart, ScheduleForecastReport)
from .configuration import Configuration
from .stock import ShipmentInternal
from .inspection import (InspectionTemplate, InspectionTemplateLine, Inspection,
    InspectionLine, InspectionGroup, InspectionReport)
from . import dash


def register():
    Pool.register(
        Configuration,
        InspectionGroup,
        Brand,
        Classification,
        ShipmentInternal,
        EquipmentCategory,
        EquipmentKind,
        PlanningCode,
        MaintenanceLocation,
        Equipment,
        EquipmentOperationCard,
        TypeActivity,
        MaintenanceEquipmentPart,
        MaintenanceEquipmentProduct,
        Period,
        Planning,
        PlanningLine,
        RequestService,
        Schedule,
        InspectionTemplate,
        InspectionTemplateLine,
        Inspection,
        InspectionLine,
        CheckListService,
        CheckListEquipment,
        CreateForecastStart,
        FrecuencyKind,
        LineFrecuency,
        PlanningCodeFrecuencyKind,
        AddLastMaintenanceDateStart,
        ScheduleForecastStart,
        VehiclePoolStart,
        VehicleTechMechanical,
        InvoiceLineRequestService,
        EquipmentInsurance,
        EventEquipment,
        dash.DashApp,
        dash.AppMaintenance,
        module='maintenance', type_='model')
    Pool.register(
        CreateForecast,
        CreateScheduling,
        AddLastMaintenanceDate,
        ScheduleForecast,
        VehiclePool,
        module='maintenance', type_='wizard')
    Pool.register(
        InspectionReport,
        RequestServiceReport,
        ScheduleForecastReport,
        VehiclePoolReport,
        module='maintenance', type_='report')
