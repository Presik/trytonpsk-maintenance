# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import If, Eval
from trytond.transaction import Transaction


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Maintenance Configuration'
    __name__ = 'maintenance.configuration'
    maintenance_sequence = fields.Many2One('ir.sequence',
        'Maintenance Reference Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', 0),
                    None]),
        ], required=True)
    inspection_sequence = fields.Many2One('ir.sequence',
        'Maintenance Inspection Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', 0),
                    None]),
        ], required=True)
    days_limit_schedule = fields.Integer('Days Limit Schedule')
    from_location_request = fields.Many2One('stock.location', "Default From Location RS")
    to_location_request = fields.Many2One('stock.location', "Default To Location RS")
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
            ], select=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')
