# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import datetime

from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateAction
from io import BytesIO


# Size photo default
WIDTH = 250
HEIGHT = 250

_STATES_VEHICLE = {
    'invisible': Eval('type_device') != 'vehicle',
    'readonly': ~Eval('active', True),
}

STATES = {
    'readonly': ~Eval('active', True),
}


class Equipment(ModelSQL, ModelView):
    "Equipment and Instrument"
    __name__ = 'maintenance.equipment'
    _rec_name = 'product'
    product = fields.Many2One('product.template', 'Product',
            domain=[('type', 'in', ['assets', 'goods'])], required=True)
    active = fields.Boolean('Active')
    model_num = fields.Char('Model Number', states=STATES)
    code = fields.Char('Code', depends=['product'])
    external_code = fields.Char('External Code', states=STATES)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    brand = fields.Many2One('maintenance.equipment.brand', 'Brand',
            states=STATES)
    serial = fields.Char('Serial', states=STATES)
    reference = fields.Char('Reference / Line')
    criticality = fields.Selection([
            ('low', 'Low'),
            ('middle', 'Middle'),
            ('high', 'High')
        ], 'Criticality', states=STATES)
    purchase_date = fields.Date('Purchase Date', states=STATES)
    capacity = fields.Char('Capacity', states=STATES)
    location = fields.Many2One('maintenance.location', 'Location',
            states=STATES)
    type_device = fields.Selection([
            ('equipment', 'Equipment'),
            ('instrument', 'Instrument'),
            ('vehicle', 'Vehicle'),
            ], 'Type Device', required=True, states=STATES)
    state = fields.Selection([
            ('excellent', 'Excellent'),
            ('good', 'Good'),
            ('acceptable', 'Acceptable'),
            ('bad', 'Bad'),
        ], 'State', required=True, states=STATES)
    parent = fields.Many2One('maintenance.equipment','Parent',
            select=True)
    tolerance = fields.Char('Tolerance', states={
            'invisible': Eval('type_device') != 'instrument',
            'readonly': ~Eval('active', True),
        })
    instruments = fields.One2Many('maintenance.equipment', 'parent',
        string='Equipments Associated',
        states={
            'invisible': (Eval('type_device') == 'instrument'),
            'readonly': ~Eval('active', True),
        })
    parts = fields.Many2Many('maintenance.equipment.part', 'equipment',
        'part', 'Parts', domain=[
            ('type', '=', 'goods'),
        ], states=STATES)
    planning = fields.One2Many('maintenance.planning', 'equipment',
            'Planning',  states=STATES)
    check_list = fields.One2Many('maintenance.check_list_equipment',
        'equipment', 'Check List Equipment', states=STATES)
    photo = fields.Binary('Photo', states=STATES)
    party = fields.Many2One('party.party', 'Owner', select=True,
         states=STATES)
    administrator = fields.Many2One('party.party', 'Administrator',
        select=True, states=STATES)
    mnt_supplier = fields.Many2One('party.party', 'Maintenance Supplier',
        select=True,  states=STATES)
    notes = fields.Text('Notes', states=STATES)
    guarantee = fields.Date('Guarantee',  states=STATES)
    kind = fields.Many2One('maintenance.equipment.kind', 'Kind',
        select=True)
    insurances = fields.One2Many('maintenance.equipment.insurance',
        'equipment', 'Insurances', states=STATES)
    work_zone = fields.Char('Work Zone', states=STATES)
    contract_party = fields.Many2One('party.party', 'Contract Party',
        select=True,  states=STATES)
    contract_date = fields.Date('Contract Date',  states=STATES)
    fee = fields.Numeric('Fee', states=STATES)
    color = fields.Char('Color', states=STATES)
    classification = fields.Many2One('maintenance.equipment.classification',
        'Classification', states=STATES)

    #-----------------------  Vehicle Fields -----------------------
    vin = fields.Char('VIN', states=_STATES_VEHICLE)
    operational_range = fields.Char('Operatinal Range',
        states=_STATES_VEHICLE)
    traffic_license_date = fields.Date('Traffic License Date',
        states=_STATES_VEHICLE)
    traffic_license_expire = fields.Date('Traffic License Expire',
        states=_STATES_VEHICLE)
    traffic_license_number = fields.Char('Traffic License Number',
        states=_STATES_VEHICLE)
    traffic_license_office = fields.Char('Traffic License Office',
        states=_STATES_VEHICLE)
    internal_number = fields.Char('Internal Number',
        states=_STATES_VEHICLE)
    motor = fields.Char('Motor', states=_STATES_VEHICLE)
    level_service = fields.Char('Level Service', states=_STATES_VEHICLE)
    cilinder_capacity = fields.Char('Cilinder Capacity', states=_STATES_VEHICLE)
    bodywork = fields.Char('Bodywork', states=_STATES_VEHICLE)
    chassis_number = fields.Char('Chassis Number', states=_STATES_VEHICLE)
    operation_card = fields.One2Many('maintenance.equipment.operation_card',
        'equipment', 'Operation Card', states=_STATES_VEHICLE)
    techmechanicals = fields.One2Many('maintenance.equipment.techmechanical',
        'equipment', 'Techmechanical', states=_STATES_VEHICLE)
    current_insurance = fields.Function(fields.Many2One(
        'maintenance.equipment.insurance', 'Insurance'),
        'get_current_insurance')
    current_operation_card = fields.Function(fields.Many2One(
        'maintenance.equipment.operation_card', 'Current Operation Card'),
        'get_current_operation_card')
    current_techmec = fields.Function(fields.Many2One(
        'maintenance.equipment.techmechanical', 'Current Techmechanical',
        depends=['techmechanicals'], states=_STATES_VEHICLE),
        'get_current_techmec')
    category = fields.Many2One('maintenance.equipment.category', 'Category',
        states=STATES)
    services = fields.Many2Many('maintenance.equipment-product.product',
        'equipment', 'product', 'Services', states=STATES)
    insurance_policy_date = fields.Date('Insurance Policy Date',
        states=_STATES_VEHICLE)
    insurance_policy_cancel = fields.Date('Insurance Policy Cancel',
        states=_STATES_VEHICLE)
    insurance_policy_number = fields.Char('Insurance Policy Number',
        states=_STATES_VEHICLE)
    second_insurance_policy_number = fields.Char('Second Insurance Policy Number',
        states=_STATES_VEHICLE)
    insurance_policy_party = fields.Many2One('party.party', 'Policy Party',
        select=True,  states=STATES)
    insurance_policy_expiration = fields.Function(fields.Integer(
            'Insurance Policy Expiration', states=_STATES_VEHICLE),
            'get_insurance_policy_expiration')
    type_motor = fields.Selection([
            ('', ''),
            ('gasoline', 'Gasoline'),
            ('diesel', 'Diesel'),
            ('gas', 'Gas'),
            ('electric', 'Electric'),
            ('hybrid', 'Hybrid'),
        ], 'Type Motor')
    type_motor_string = type_motor.translated('type_motor')
    link_date = fields.Date('Link Date',  states=STATES)
    unlink_date = fields.Date('Unlink Date',  states=STATES)
    gps_serial = fields.Char('Gps Serial', states=_STATES_VEHICLE)
    gps_model = fields.Char('Gps Model', states=_STATES_VEHICLE)
    gps_imei = fields.Char('Gps IMEI', states=_STATES_VEHICLE)
    simcard = fields.Char('Simcard', states=_STATES_VEHICLE)
    # notification_documents = fields.One2Many('notification.document',
    #     'origin',  string='Notification Documents')
    number_door = fields.Char('Number Door')
    limit_property = fields.Char('Limit Property')
    number_registration = fields.Char('Number Registration')
    date_registration = fields.Date('Date Registration')
    #Events
    events= fields.One2Many('maintenance.event_equipment',
        'equipment', 'Events Equipment', states=STATES)
    request_services= fields.Function(fields.One2Many('maintenance.request_service',
        None, 'Request Services'), 'get_request_services')

    @classmethod
    def __setup__(cls):
        super(Equipment, cls).__setup__()

    def get_insurance_policy_expiration(self, name):
        res = None
        if self.insurance_policy_date:
            today = datetime.date.today()
            res = (self.insurance_policy_date - today).days
        return res

    def get_rec_name(self, name):
        rec_name = self.product.rec_name
        if self.code:
            rec_name = '[' + self.code + ']' + ' ' + rec_name
        return (rec_name)

    def get_request_services(self, name):
        Service = Pool().get('maintenance.request_service')
        services = Service.search([
            ('equipment', '=', self.id),
            ('state', 'in', ['open', 'assigned', 'approved', 'done']),
        ], order=[('request_date', 'DESC')])
        return [s.id for s in services]


    def get_image(self):
        import io
        if self.photo:
            img = self.photo
            stream_str = io.BytesIO(img)
            return [stream_str.getvalue(), 'image/png']

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [
            bool_op,
            ('code',) + tuple(clause[1:]),
            ('external_code',) + tuple(clause[1:]),
            (cls._rec_name,) + tuple(clause[1:]),
        ]

    @staticmethod
    def default_criticality():
        return 'low'

    @staticmethod
    def default_type_device():
        return 'equipment'

    @staticmethod
    def default_state():
        return 'good'

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('product', 'code', 'active')
    def on_change_product(self):
        if self.product:
            self.code = self.product.products[0].code
            self.active = self.product.active
        else:
            self.code = None

    def get_current_operation_card(self, name=None):
        res = None
        opcs = dict([(oc.expire_date, oc.id) for oc in self.operation_card])
        if opcs:
            res = opcs[max(opcs.keys())]
        return res

    def get_current_techmec(self, name=None):
        res = None
        tmcs = dict([(tm.expire_date, tm.id) for tm in self.techmechanicals])
        if tmcs:
            res = tmcs[max(tmcs.keys())]
        return res

    def get_current_insurance(self, name=None):
        res = None
        records = dict([(rec.insurance_date, rec.id) for rec in self.insurances])
        if records:
            res = records[max(records.keys())]
        return res

    def get_image_photo(self):
        if self.photo:
            # img = qrcode.make(self.qr_link_face)
            buffered = BytesIO()
            self.photo.save(buffered, 'PNG')
            img_str = buffered.getvalue()
            return [img_str, 'image/png']


class MaintenanceEquipmentPart(ModelSQL):
    'Maintenance Equipment - Part'
    __name__ = 'maintenance.equipment.part'
    _table = 'maintenance_equipment_part'
    equipment = fields.Many2One('maintenance.equipment', 'Maintenance Equipment',
            ondelete='CASCADE', select=True, required=True)
    part = fields.Many2One('product.template', 'Product', ondelete='RESTRICT',
            domain=[('type', '=', 'goods')], select=True, required=True)


class MaintenanceEquipmentProduct(ModelSQL):
    'Maintenance Equipment - Product'
    __name__ = 'maintenance.equipment-product.product'
    _table = 'maintenance_equipment_product_product'
    equipment = fields.Many2One('maintenance.equipment',
        'Maintenance Equipment', ondelete='CASCADE',
        select=True, required=True)
    product = fields.Many2One('product.template', 'Product',
        ondelete='RESTRICT', select=True, required=True)


class EquipmentOperationCard(ModelSQL, ModelView):
    'Equipment Operation Card'
    __name__ = 'maintenance.equipment.operation_card'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            ondelete='CASCADE', required=True)
    date = fields.Date('Date', states=STATES, required=True)
    expire_date = fields.Date('Expire Date', states=STATES,
            required=True)
    card_number = fields.Char('Card Number', required=True)
    notes = fields.Char('Notes')
    days_expiration = fields.Function(fields.Integer('Expiration'),
        'get_days_expiration')
    territorial_address = fields.Char('Territorial Address')

    @classmethod
    def __setup__(cls):
        super(EquipmentOperationCard, cls).__setup__()
        cls._order.insert(0, ('expire_date', 'DESC'))

    def get_days_expiration(self, name):
        res = None
        if self.expire_date:
            today = datetime.date.today()
            res = (self.expire_date - today).days
        return res


class VehicleTechMechanical(ModelSQL, ModelView):
    'Vehicle TechMechanical'
    __name__ = 'maintenance.equipment.techmechanical'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            ondelete='CASCADE', required=True)
    date = fields.Date('Date', states=STATES,)
    expire_date = fields.Date('Expire Date', states=STATES,
            required=True)
    party = fields.Many2One('party.party', 'Party', select=True)
    number = fields.Char('Number')
    notes = fields.Char('Notes')
    days_expiration = fields.Function(fields.Integer('Expiration'),
        'get_days_expiration')
    number_runt = fields.Char('Number Runt')
    certificate_accreditation = fields.Char('Certificate Accreditation')

    @classmethod
    def __setup__(cls):
        super(VehicleTechMechanical, cls).__setup__()
        cls._order.insert(0, ('expire_date', 'DESC'))

    def get_days_expiration(self, name):
        res = None
        if self.expire_date:
            today = datetime.date.today()
            res = (self.expire_date - today).days
        return res


class EquipmentInsurance(ModelSQL, ModelView):
    'Equipment Insurance'
    __name__ = 'maintenance.equipment.insurance'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
        ondelete='CASCADE', required=True)
    name = fields.Char('Name', required=False)
    number = fields.Char('Insurance Number', required=True)
    insurance_date = fields.Date('Insurance Date', required=False)
    insurer_company = fields.Many2One('party.party',
        'Insurer Company', required=True)
    expire_date = fields.Date('Expire Date', required=True)
    days_expiration = fields.Function(fields.Integer('Expiration'),
        'get_days_expiration')

    @classmethod
    def __setup__(cls):
        super(EquipmentInsurance, cls).__setup__()
        cls._order.insert(0, ('insurance_date', 'DESC'))

    def get_days_expiration(self, name):
        res = None
        if self.expire_date:
            today = datetime.date.today()
            res = (self.expire_date - today).days
        return res


class CheckListEquipment(ModelSQL, ModelView):
    'Check List Equipment'
    __name__ = 'maintenance.check_list_equipment'
    element = fields.Char('Element')
    quantity = fields.Numeric('Quantity', required=False)
    notes = fields.Char('Notes')
    checked = fields.Boolean('Checked')
    equipment = fields.Many2One('maintenance.equipment', 'Equipment')

    @classmethod
    def __setup__(cls):
        super(CheckListEquipment, cls).__setup__()


class EquipmentKind(ModelSQL, ModelView):
    'Equipment Kind'
    __name__ = 'maintenance.equipment.kind'
    name = fields.Char('Name', required=True)


class EquipmentCategory(ModelSQL, ModelView):
    'Equipment Category'
    __name__ = 'maintenance.equipment.category'
    name = fields.Char('Name', required=True)


class VehiclePoolStart(ModelView):
    'Vehicle Pool Start'
    __name__ = 'maintenance.vehicle_pool.start'
    category = fields.Many2One('maintenance.equipment.category', 'Category')
    kind = fields.Many2One('maintenance.equipment.kind', 'Kind')
    brand = fields.Many2One('maintenance.equipment.brand', 'Brand')


class VehiclePool(Wizard):
    'Equipment Pool'
    __name__ = 'maintenance.vehicle_pool'
    start = StateView('maintenance.vehicle_pool.start',
        'maintenance.vehicle_pool_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateAction('maintenance.report_vehicle_pool')

    def do_print_(self, action):
        category_id = None
        kind_id = None

        if self.start.category:
            category_id = self.start.category.id
        if self.start.kind:
            kind_id = self.start.kind.id
        data = {
            'category': category_id,
            'kind': kind_id,
            'brand': self.start.brand.id if self.start.brand else None,
        }
        return action, data


class VehiclePoolReport(Report):
    __name__ = 'maintenance.vehicle_pool_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Equipment = Pool().get('maintenance.equipment')
        dom_vehicles = [
            ('type_device', '=', 'vehicle'),
        ]
        if data['category']:
            dom_vehicles.append(('category', '=', data['category']))
        if data['kind']:
            dom_vehicles.append(('kind', '=', data['kind']))
        if data['brand']:
            dom_vehicles.append(('brand', '=', data['brand']))

        report_context['records'] = Equipment.search(dom_vehicles,
            order=[('code', 'ASC')])
        return report_context


class Brand(ModelSQL, ModelView):
    "Brand"
    __name__ = "maintenance.equipment.brand"
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(Brand, cls).__setup__()


class Classification(ModelSQL, ModelView):
    "Classification"
    __name__ = "maintenance.equipment.classification"
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(Classification, cls).__setup__()


# class NotificationDocument(metaclass=PoolMeta):
#     __name__ = 'notification.document'
#
#     @classmethod
#     def _get_origin(cls):
#         return super(NotificationDocument, cls)._get_origin() + ['maintenance.equipment']


class EventEquipment(ModelSQL, ModelView):
    'Event Equipment'
    __name__ = 'maintenance.event_equipment'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment')
    type_event = fields.Char('Type Event')
    date_event = fields.Date('Date Event')
    hour_event = fields.Time('Hour Event')
    number_person = fields.Numeric('Number Person')
    address_event = fields.Char('Address Event')
    description_event = fields.Char('Description Event')
    maintenance_executed = fields.Char('Maintenance Executed')

    @classmethod
    def __setup__(cls):
        super(EventEquipment, cls).__setup__()


class EquipmentReport(Report):
    'Equipment Report'
    __name__ = 'maintenance.equipment.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(EquipmentReport, cls).get_context(records, data)
        Company = Pool().get('company.company')
        User = Pool().get('res.user')
        company_id = Transaction().context.get('company')
        user_id = Transaction().context.get('user')
        report_context['user'] = User(user_id)
        report_context['company'] = Company(company_id)
        return report_context
