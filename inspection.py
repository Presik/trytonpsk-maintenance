# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, date, timedelta
from trytond.report import Report
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.pyson import Eval, If, In, Get
from trytond.pool import Pool
from trytond.transaction import Transaction


STATES = {'readonly': (Eval('state') != 'draft')}

RESPONSE = {
        'numeric_1_5': [(str(n+1), str(n+1)) for n in range(5)],
        'yes_no': [
            ('yes', 'Yes'),
            ('no', 'No'),
            ('not_apply', 'Not Apply'),
        ],
}


class InspectionGroup(ModelSQL, ModelView):
    "Inspection Group"
    __name__ = "maintenance.inspection.group"
    name = fields.Char('Name', required=True, select=True)
    code = fields.Char('Code', select=True)
    description = fields.Text('Description', select=True)


class InspectionTemplate(ModelSQL, ModelView):
    "Inspection Template"
    __name__ = "maintenance.inspection_template"
    name = fields.Char('Name', required=True, select=True)
    code = fields.Char('Code', required=True, select=True)
    active = fields.Boolean('Active')
    correction_time = fields.Integer('Correction Time', help='In Days')
    frecuency = fields.Integer('Frecuency', help='In Days')
    lines = fields.One2Many('maintenance.inspection_template.line',
        'template', 'Lines')


class InspectionTemplateLine(ModelSQL, ModelView):
    "Inspection Template Line"
    __name__ = "maintenance.inspection_template.line"
    _rec_name = 'ask'
    template = fields.Many2One('maintenance.inspection_template',
        'Template Inspection', required=True)
    group = fields.Many2One('maintenance.inspection.group',
        'Inspection Group', required=False)
    sequence = fields.Integer('Sequence', required=True, select=True)
    ask = fields.Char('Ask', required=True, select=True)
    type_response = fields.Selection([
        ('numeric_1_5', 'Numeric 1-5'),
        ('free_text', 'Free Text'),
        ('yes_no', 'Yes or No'),
    ], 'Type Response', required=True, select=True)

    @staticmethod
    def default_type_response():
        return 'yes_no'


class Inspection(Workflow, ModelSQL, ModelView):
    "Inspection"
    __name__ = "maintenance.inspection"
    number = fields.Char('Number', readonly=True, select=True)
    party = fields.Many2One('party.party', 'Party', states=STATES)
    operator = fields.Many2One('party.party', 'Operator', states=STATES)
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
        required=True, states=STATES)
    date_time = fields.DateTime('Date', required=True, states=STATES)
    template = fields.Many2One('maintenance.inspection_template',
        'Inspection Template', required=True, states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[('id', If(In('company',
        Eval('context', {})), '=', '!='), Get(Eval('context', {}),
        'company', 0)), ], states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('process', 'Process'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    lines = fields.One2Many('maintenance.inspection.line', 'inspection',
        'Lines', states=STATES)
    notes = fields.Text('Notes', states=STATES)
    next_inspection = fields.Date('Next Inspection', states={
            'readonly': True
        }, depends=['template', 'date_time'])
    validity_time = fields.Function(fields.Integer('Validity Time'),
        'get_validity_time')
    result = fields.Selection([
        ('approved', 'Approved'),
        ('no_approved', 'No Approved'),
        ], 'Result', states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state') == 'done',
        }, depends=['state'])
    result_string = result.translated('result')

    @classmethod
    def __setup__(cls):
        super(Inspection, cls).__setup__()
        cls._order.insert(0, ('number', 'ASC'))
        cls._transitions |= set((
            ('draft', 'process'),
            ('process', 'done'),
            ('process', 'cancelled'),
            ('cancelled', 'draft'),
            ('process', 'draft'),
            ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done']),
                },
            'cancel': {
                'invisible': Eval('state') != 'process',
                },
            'done': {
                'invisible': Eval('state') != 'process',
                },
            'process': {
                'invisible': Eval('state') != 'draft',
                },
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_result():
        return 'no_approved'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date_time():
        return datetime.now()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('process')
    def process(cls, records):
        cls.set_number(records)

    def get_validity_time(self, name=None):
        res = 0
        delta = date.today() - self.date_time.date()
        if self.template.frecuency:
            res = self.template.frecuency - delta.days
        if res > 0:
            return res
        return 0

    @fields.depends('template', 'date_time')
    def on_change_next_inspection(self):
        if self.template and self.template.frecuency and self.date_time:
            self.next_inspection = self.date_time.date() + timedelta(self.template.frecuency)

    @fields.depends('template', 'lines')
    def on_change_template(self):
        lines_to_add = []
        if self.template:
            sequence = 1
            for tline in self.template.lines:
                lines_to_add.append({
                    'sequence': sequence,
                    'line_ask': tline.id,
                })
                sequence += 1
        self.lines = lines_to_add

    @classmethod
    def set_number(cls, records):
        '''
        Fill the number field with the inspection sequence
        '''
        pool = Pool()
        Config = pool.get('maintenance.configuration')
        config = Config(1)

        for record in records:
            if record.number:
                continue
            if not config.inspection_sequence:
                continue
            number = config.inspection_sequence.get()
            cls.write([record], {'number': number})


class InspectionLine(ModelSQL, ModelView):
    "Inspection Line"
    __name__ = "maintenance.inspection.line"
    inspection = fields.Many2One('maintenance.inspection',
        'Inspection', required=True)
    sequence = fields.Integer('Sequence', required=True)
    line_ask = fields.Many2One('maintenance.inspection_template.line',
        'Inspection Line', required=True)
    response = fields.Selection('selection_response', 'Response',
        depends=['line_ask'])
    comments = fields.Text('Comments')
    group = fields.Many2One('maintenance.inspection.group',
        'Inspection Group', states={
            'readonly': True
        })

    @classmethod
    def __setup__(cls):
        super(InspectionLine, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))

    @fields.depends('line_ask', 'response')
    def selection_response(self):
        res = [('', '')]
        if self.line_ask and self.line_ask.type_response in ('numeric_1_5', 'yes_no'):
            res = RESPONSE.get(self.line_ask.type_response)
        return res

    @fields.depends('line_ask', 'group')
    def on_change_with_line_ask(self):
        if self.line_ask and self.line_ask.group:
            if self.line_ask.type_response == 'yes_no':
                return 'yes'
            elif self.line_ask.type_response == 'numeric_1_5':
                return '5'

    @fields.depends('line_ask', 'group')
    def on_change_with_group(self):
        if self.line_ask and self.line_ask.group:
            return self.line_ask.group.id


class InspectionReport(Report):
    "Inspection Report"
    __name__ = "maintenance.inspection"
