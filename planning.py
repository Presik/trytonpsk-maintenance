# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, timedelta
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, Id, If
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.transaction import Transaction

STATES = {'readonly': (Eval('state') != 'draft')}


class Period(Workflow, ModelSQL, ModelView):
    'Maintenance Period'
    __name__ = 'maintenance.period'
    name = fields.Char('Name Period', required=True, states=STATES)
    reference = fields.Char('Reference', required=True, states=STATES)
    start_date = fields.Date('Start Date', required=True, states=STATES)
    end_date = fields.Date('End Date', required=True, states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('closed', 'Closed'),
        ('cancel', 'Cancel'),
        ], 'State', readonly=True, required=True)

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._order.insert(0, ('start_date', 'ASC'))
        # cls._constraints += [
        #     ('overlap_date', 'periods_overlaps'),
        #     ('check_dates', 'wrong_date'),
        # ]
        # cls._error_messages.update({
        #         'periods_overlaps': 'You can not have two overlapping periods!',
        #         'wrong_date': 'The start date is greater or equal than end date \n'
        #                       'or start date is smaller that today!'
        #         })
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'closed'),
            ('open', 'draft'),
            ('draft', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'closed': {
                'invisible': Eval('state') != 'open',
            },
            'open': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, records):
        pass

    def _get_last_date(self, equipment, line):
        pool = Pool()
        last_date = None
        last_plan_date = None
        Request = pool.get('maintenance.request_service')
        Forecast = pool.get('maintenance.schedule')

        requests = Request.search([
                ('equipment', '=', equipment),
                ('schedules.line', '=', line),
                ('state', '!=', 'cancel')
                ])

        forecasts = Forecast.search([
                ('equipment', '=', equipment),
                ('line', '=', line),
                ])

        if requests:
            for req in requests:
                if not last_date or req.effective_date > last_date:
                    last_date = req.effective_date
            if isinstance(last_date, datetime):
                last_date = last_date.date()

        if forecasts:
            for forecast in forecasts:
                if not last_plan_date or forecast.planning_date > last_plan_date:
                    last_plan_date = forecast.planning_date
            if not last_date or last_date < last_plan_date:
                last_date = last_plan_date
        return last_date

    def _get_plan_date(self, period, line, last_date, start_date=None):
        plan_date = []
        frecuency = timedelta(float(line.frecuency))
        if isinstance(last_date, datetime):
            last_date = last_date.date()

        if not last_date and start_date:
            last_date = start_date

        if not last_date or (last_date + frecuency) < self.start_date:
            last_date = self.start_date

        while (last_date + frecuency) <= period.end_date:
            last_date = last_date + frecuency
            plan_date.append(last_date)
        return plan_date

    @classmethod
    def check_dates(cls, plan_periods):
        """
        now = datetime.now().date()
        for period in plan_periods:
            if period.start_date >= period.end_date or period.start_date <= now:
                return False
        """
        return True

    @classmethod
    def overlap_date(cls, plan_periods):
        new_period = plan_periods[0]
        old_periods = cls.search([('id', '!=', new_period.id)])
        for old_period in old_periods:
            if (
                new_period.start_date <= old_period.start_date and \
                new_period.end_date >= old_period.start_date) \
                or (
                new_period.start_date >= old_period.start_date and \
                new_period.start_date <= old_period.end_date \
                ):
                return False
        return True

    @classmethod
    def create_forecast(cls, planning, period, start_date=None):
        pool = Pool()
        Forecast = pool.get('maintenance.schedule')

        lines_to_create = []
        for line in planning.lines:
            last_date = period._get_last_date(planning.equipment, line.code)
            plan_date = period._get_plan_date(period, line, last_date)
            if not plan_date:
                continue
            for new_date in plan_date:
                lines_to_create.append({
                    'plan_period': period.id,
                    'planning': planning.id,
                    'equipment': planning.equipment,
                    'description': line.description,
                    'line': line.id,
                    'code': line.code.id,
                    'last_date': last_date,
                    'work_forecast_hours': line.work_forecast_hours,
                    'planning_date': new_date,
                    'state': 'open',
                    })
        Forecast.create(lines_to_create)


class Planning(ModelSQL, ModelView):
    'Maintenance Planning'
    __name__ = 'maintenance.planning'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            select=True, required=True)
    type_activity = fields.Many2One('maintenance.type_activity',
            'Type Activity', required=True)
    notes = fields.Text('Notes')
    party = fields.Many2One('party.party', 'Party', select=True)
    lines = fields.One2Many('maintenance.planning.line', 'planning',
            'Lines')

    @classmethod
    def __setup__(cls):
        super(Planning, cls).__setup__()

    @staticmethod
    def default_type_activity():
        TypeActivity = Pool().get('maintenance.type_activity')
        activities = TypeActivity.search([
            ('activity', '=', 'preventive'),
        ])
        if activities:
            return activities[0].id


class PlanningLine(ModelSQL, ModelView):
    'Maintenance Planning Line'
    __name__ = 'maintenance.planning.line'
    _rec_name = 'name'
    planning = fields.Many2One('maintenance.planning', 'Planning',
            ondelete='CASCADE', required=True)
    name = fields.Char('Name', select=True, required=True)
    code = fields.Many2One('maintenance.planning_code', 'Code',
            required=True)
    description = fields.Text('Description', required=True)
    frecuencies_lines = fields.One2Many('maintenance.planning.line.frecuency',
            'line', 'Frecuencies Lines')
    work_forecast_hours = fields.Numeric('Work Forecast Hours')
    party = fields.Many2One('party.party', 'Party', select=True)
    last_maintenance = fields.Date('Last Maintenance', select=True)
    days_limit_schedule = fields.Integer('Days Limit Schedule')

    @classmethod
    def __setup__(cls):
        super(PlanningLine, cls).__setup__()
        cls._buttons.update({
                'create_frecuencies': {
                    'invisible': Eval('frecuencies_lines', [0]),
                },
        })

    @fields.depends('code', 'description', 'frecuencies_lines')
    def on_change_code(self):
        if not self.code:
            return
        self.name = self.code.name
        if self.code.description:
            self.description = self.code.description

    @classmethod
    @ModelView.button
    def create_frecuencies(cls, lines):
        values_to_create = []
        for line in lines:
            if line.code:
                for f in line.code.frecuencies:
                    val = {
                        'frecuency_kind': f.frecuency_kind.id,
                        'value': f.value,
                    }
                    values_to_create.append(val)
            if not values_to_create:
                return
        cls.write([line], {
                'frecuencies_lines': [('create', values_to_create)]
        })


class FrecuencyKind(ModelSQL, ModelView):
    'Frecuency Kind'
    __name__ = 'maintenance.planning.frecuency_kind'
    name = fields.Char('Name', select=True, required=True)
    code = fields.Char('Code', required=True)
    uom_frecuency = fields.Many2One('product.uom', 'UoM Frecuency',
        required=True, domain=[
            If(Eval('source_data') == 'clock',
                ('id', 'in', [
                        Id('product', 'uom_hour'),
                        Id('product', 'uom_day'),
                ]), ())
                ], depends=['clock'])
    source_data = fields.Selection([
            ('clock', 'Clock'),
            ], 'Source Data', required=True)

    @classmethod
    def __setup__(cls):
        super(FrecuencyKind, cls).__setup__()

    @staticmethod
    def default_source_data():
        return 'clock'


class LineFrecuency(ModelSQL, ModelView):
    'Line Frecuency'
    __name__ = 'maintenance.planning.line.frecuency'
    line = fields.Many2One('maintenance.planning.line', 'Line',
            ondelete='CASCADE', select=True, required=True)
    frecuency_kind = fields.Many2One('maintenance.planning.frecuency_kind',
            'Frecuency Kind', select=True, required=True)
    value = fields.Integer('Value', required=True)
    note = fields.Text('Notes')


    @classmethod
    def __setup__(cls):
        super(LineFrecuency, cls).__setup__()


class PlanningCode(ModelSQL, ModelView):
    'Maintenance Planning Code'
    __name__ = 'maintenance.planning_code'
    _rec_name = 'code'
    name = fields.Char('Name', select=True, required=True)
    code = fields.Char('Code', select=True, required=True)
    description = fields.Text('Description')
    type_ = fields.Selection([
            ('root', 'Root'),
            ('normal', 'Normal'),
            ], 'Type', required=True)
    parent = fields.Many2One('maintenance.planning_code', 'Parent',
            domain=[
                ('type_','=','root')
            ], states={
                'invisible': Eval('type_') == 'root',
            }, select=True)
    childs = fields.One2Many('maintenance.planning_code', 'parent',
            'Children', states={
                'invisible': Eval('type_') != 'root',
            })
    frecuencies = fields.One2Many('maintenance.planning_code.frecuency_kind',
            'code', 'Default Frecuencies', states={
                'invisible': Eval('type_') == 'root',
            })

    @staticmethod
    def default_type_():
        return 'normal'


class PlanningCodeFrecuencyKind(ModelSQL, ModelView):
    'Planning Code - Planning Frecuency Kind'
    __name__ = 'maintenance.planning_code.frecuency_kind'
    code = fields.Many2One('maintenance.planning_code',
            'Code', select=True, required=True, ondelete='CASCADE')
    frecuency_kind = fields.Many2One('maintenance.planning.frecuency_kind',
            'Frecuency Kind', required=True)
    value = fields.Integer('Value', required=False)


class CreateForecastStart(ModelView):
    'Create Forecast Start'
    __name__ = 'maintenance.create_forecast.start'
    period = fields.Many2One('maintenance.period', 'Period',
            domain=[
                ('state', '=', 'open'),
            ], required=True)
    start_date = fields.Date('Start Date')


class CreateForecast(Wizard):
    'Create Forecast'
    __name__ = 'maintenance.create_forecast'
    start = StateView('maintenance.create_forecast.start',
        'maintenance.create_forecast_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        Planning = Pool().get('maintenance.planning')
        Period = Pool().get('maintenance.period')
        period = Period(self.start.period)
        ids = Transaction().context['active_ids']
        plannings = Planning.browse(ids)
        for plan in plannings:
            start_date = self.start.start_date
            Period.create_forecast(plan, period, start_date=start_date)
        return 'end'


class AddLastMaintenanceDateStart(ModelView):
    'Add Last Maintenance Date Start'
    __name__ = 'maintenance.add_last_maintenance_date.start'
    maintenance_date = fields.Date('Maintenance Date', required=True)


class AddLastMaintenanceDate(Wizard):
    'Add Last Maintenance Date'
    __name__ = 'maintenance.add_last_maintenance_date'
    start = StateView('maintenance.add_last_maintenance_date.start',
        'maintenance.add_last_maintenance_date_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        Planning = Pool().get('maintenance.planning')
        ids = Transaction().context['active_ids']
        plannings = Planning.browse(ids)
        for planning in plannings:
            PlanningLine.write(list(planning.lines), {
                'last_maintenance': self.start.maintenance_date
            })
        return 'end'
