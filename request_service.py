# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import datetime, date
from decimal import Decimal

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Not, Equal, Eval, Or, If, In, Get, And
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond import backend


STATES_USER = {
    'readonly': (Eval('state') != 'open'),
}

STATES_ADMIN = {
    'readonly': Not(In(Eval('state'), ['approved', 'assigned', 'waiting_approval'])),
}
_ZERO = Decimal(0)


class RequestService(Workflow, ModelSQL, ModelView):
    'Request Service'
    __name__ = 'maintenance.request_service'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True, select=True)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES_USER, domain=[('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0)), ])
    request_date = fields.DateTime('Request Date', states=STATES_ADMIN,
            required=True)
    repair_time = fields.Numeric('Repair Time', help="In hours",
        states=STATES_ADMIN)
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
         required=True)
    location = fields.Many2One('maintenance.location', 'Location',
        depends=['equipment'], states={'readonly': Or(
            (Eval('create_uid') != Eval('_user', '0')),
            (Eval('state') != 'open')),
        })
    description = fields.Text('Description', required=True,
        )
    type_activity = fields.Many2One('maintenance.type_activity',
        'Type Activity', states=STATES_ADMIN)
    action = fields.Text('Action', states=STATES_ADMIN)
    code = fields.Many2One('maintenance.planning_code', 'Code',
        readonly=True, states=STATES_ADMIN)
    period = fields.Char('Period', readonly=True, states=STATES_ADMIN)
    assigned_to = fields.Many2One('party.party', 'Party', select=True,
        states=STATES_ADMIN)
    type_service = fields.Selection([
        ('internal', 'Internal'),
        ('external', 'External'),
        ('both', 'Both'),
        ], 'Type Service', states=STATES_ADMIN)
    type_service_string = type_service.translated('type_service')
    priority = fields.Selection([
        ('urgent', 'Urgent'),
        ('important', 'Important'),
        ('low', 'Low'),
        ], 'Priority', required=True, states=STATES_USER)
    priority_string = priority.translated('priority')
    effective_date = fields.DateTime('Effective Datetime',
        states={'readonly': Not(In(Eval('state'),
                ['approved', 'assigned', 'waiting_approval'])),
            'required': Equal(Eval('state'), 'done')})
    notes = fields.Char('Notes', states=STATES_ADMIN)
    efficacy = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
        ('', ''),
        ], 'Efficacy', states={
        'readonly': Or(
                (Eval('create_uid') != Eval('_user', '0')),
                (Eval('state') != 'done')),
        'required': Eval('state') == 'marked',
        })
    efficacy_string = efficacy.translated('efficacy')
    department = fields.Many2One('company.department', 'Department',
        states=STATES_USER)
    state = fields.Selection([
        ('open', 'Open'),
        ('assigned', 'Assigned'),
        ('cancel', 'Cancel'),
        ('waiting_approval', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('no_approved', 'No Approved'),
        ('done', 'Done'),
        ('marked', 'Marked'),
    ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    check_list = fields.One2Many('maintenance.check_list',
        'request', 'Check List', states=STATES_ADMIN)
    approved_notes = fields.Text('Approved Notes', states={
        'readonly': Eval('state') != 'waiting_approval',
    })
    odometer = fields.Integer('Odometer', states=STATES_ADMIN)
    moves = fields.One2Many('stock.move', 'sale', 'Moves',
        states=STATES_ADMIN, readonly=True)
    invoices_lines = fields.Many2Many(
        'invoice.line-maintenance.request_service',
        'request_service', 'invoice_line', 'Invoices Lines',
        domain=[
            ('invoice.type', '=', 'in_invoice'),
        ])
    total_cost = fields.Function(fields.Numeric('Total Cost',
        digits=(16, 2)), 'get_total_cost')
    origin = fields.Reference('Origin', selection='get_origin',
        select=True)
    shipments = fields.One2Many('stock.shipment.internal', 'origin',
        'Shipments', readonly=True)

    @classmethod
    def __setup__(cls):
        super(RequestService, cls).__setup__()
        cls._order.insert(0, ('request_date', 'DESC'))
        cls._transitions |= set((
                ('open', 'assigned'),
                ('open', 'cancel'),
                ('assigned', 'done'),
                ('assigned', 'waiting_approval'),
                ('waiting_approval', 'no_approved'),
                ('waiting_approval', 'approved'),
                ('approved', 'done'),
                ('done', 'marked'),
                ))
        cls._buttons.update({
            'open': {
                'invisible': Eval('state') != 'open',
                },
            'assign': {
                'invisible': Eval('state') != 'open',
                },
            'cancel': {
                'invisible': Eval('state') != 'open',
                },
            'waiting_approval': {
                'invisible': Eval('state') != 'assigned',
                },
            'approved': {
                'invisible': Eval('state') != 'waiting_approval',
                },
            'no_approved': {
                'invisible': Eval('state') != 'waiting_approval',
                },
            'done': {
                'invisible': And(
                    Eval('state') != 'assigned',
                    Eval('state') != 'approved',
                ),},
            'marked': {
                'invisible': Eval('state') != 'done',
                },
            'create_internal_shipment': {
                'invisible': False,
                },
            })

    @staticmethod
    def _get_origin():
        'Return list of Model names for origin Reference'
        return ['maintenance.schedule']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
                ('model', 'in', models),
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    def get_shipments(self, name):
        shipments = []
        return shipments

    def search_shipments(self):
        shipments = []
        return shipments

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'open'

    @staticmethod
    def default_request_date():
        return datetime.now()

    @staticmethod
    def default_priority():
        return 'low'

    @staticmethod
    def default_type_service():
        return 'internal'

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('assigned')
    def assign(cls, records):
        cls.set_number(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting_approval')
    def waiting_approval(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approved(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('no_approved')
    def no_approved(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for record in records:
            cls._close_forecast(record)

    @classmethod
    @ModelView.button
    @Workflow.transition('marked')
    def marked(cls, records):
        pass

    def get_total_cost(self, name=None):
        res = _ZERO
        for iline in self.invoices_lines:
            res += iline.amount
        for shipment in self.shipments:
            for move in shipment.moves:
                res += (Decimal(move.quantity) * move.product.cost_price)
        return res

    @classmethod
    def set_number(cls, requests):
        '''
        Fill the number field with the request sequence
        '''
        pool = Pool()
        Config = pool.get('maintenance.configuration')
        config = Config(1)

        for request in requests:
            if request.number:
                continue
            if not config.maintenance_sequence:
                continue
            number = config.maintenance_sequence.get()
            cls.write([request], {'number': number})

    @classmethod
    def set_created_by(cls, requests):
        '''
        Fill the created_by field with the request
        '''
        pool = Pool()
        User = pool.get('res.user')
        user = User.search(Transaction().user)

        for request in requests:
            cls.write(request.id, {
                'created_by': user.id,
                })

    @fields.depends('equipment', 'location')
    def on_change_equipment(self):
        self.location = None
        if self.equipment:
            if self.equipment.location:
                self.location = self.equipment.location.id

    @classmethod
    def _close_forecast(cls, record):
        Schedule = Pool().get('maintenance.schedule')
        if record.origin:
            if isinstance(record.effective_date, datetime):
                eff_date = record.effective_date.date()
            Schedule.write([record.origin], {
                    'state': 'done',
                    'exec_date': eff_date
            })

    @classmethod
    @ModelView.button_action('maintenance.act_create_internal_shipment')
    def create_internal_shipment(cls, requests):
        for request in requests:
            request._create_internal_shipment()

    def _create_internal_shipment(self):
        ShipmentInternal = Pool().get('stock.shipment.internal')
        config = Pool().get('maintenance.configuration')(1)
        today = date.today()
        values = {
            'number': self.number,
            'planned_date': today,
            'planned_start_date': today,
            'origin': str(self),
        }
        if config.from_location_request:
            values['from_location'] = config.from_location_request.id
        if config.from_location_request:
            values['to_location'] = config.to_location_request.id
        ShipmentInternal.create([values])


class RequestServiceReport(Report):
    'Request Service Report'
    __name__ = 'maintenance.request_service'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class CheckListService(ModelSQL, ModelView):
    'Check List Service'
    __name__ = 'maintenance.check_list'

    element = fields.Char('Element', required=True)
    quantity = fields.Numeric('Quantity', required=True)
    checked = fields.Boolean('Checked')
    notes = fields.Char('Notes')
    request = fields.Many2One('maintenance.request_service', 'Request')

    @classmethod
    def __setup__(cls):
        super(CheckListService, cls).__setup__()


class InvoiceLineRequestService(ModelSQL):
    'Invoice Line - Request Service'
    __name__ = 'invoice.line-maintenance.request_service'
    _table = 'invoice_line_maintenance_request_service'

    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
        required=True, select=True, ondelete='CASCADE')
    request_service = fields.Many2One('maintenance.request_service',
        'Request Service', required=True, select=True, ondelete='CASCADE')
