# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, date, time, timedelta
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, Id
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.report import Report


STATES = {'readonly': (Eval('state') != 'open')}

UTC = timedelta(0, 18000)


class Schedule(Workflow, ModelSQL, ModelView):
    'Maintenance Schedule'
    __name__ = 'maintenance.schedule'
    plan_period = fields.Many2One('maintenance.period', 'Period',
            states=STATES)
    planning = fields.Many2One('maintenance.planning', 'Planning',
            states=STATES)
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            required=True, states=STATES, depends=['line'])
    line = fields.Many2One('maintenance.planning.line', 'Line',
            required=True, states=STATES)
    code = fields.Many2One('maintenance.planning_code', 'Code',
            states=STATES, depends=['line'])
    work_forecast_hours = fields.Numeric('Work Forecast Hours',
            states=STATES)
    cause = fields.Char('Cause', states=STATES)
    description = fields.Text('Description', required=True,
            states=STATES)
    last_date = fields.Date('Last Date', states={
            'readonly': True,
            })
    planning_date = fields.Date('Planning Date', select=True,
            states=STATES)
    exec_date = fields.Date('Execution Date', states={
                'readonly': True,
            })
    request = fields.Many2One(
            'maintenance.request_service', 'Request Service',
            states={
                'readonly': True,
            })
    party = fields.Many2One('party.party', 'Party', select=True,
            states=STATES)
    state = fields.Selection([
            ('open', 'Open'),
            ('process', 'Process'),
            ('cancel', 'Cancel'),
            ('done', 'Done'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')

    @classmethod
    def __setup__(cls):
        super(Schedule, cls).__setup__()
        cls._transitions |= set((
            ('open', 'process'),
            ('process', 'done'),
            ('process', 'draft'),
            ('open', 'cancel'),
        ))
        cls._buttons.update({
            'open': {
                'invisible': Eval('state') != 'open',
                },
            'cancel': {
                'invisible': Eval('state') != 'open',
                },
            'process': {
                'invisible': Eval('state') != 'open',
                },
            'done': {
                'invisible': Eval('state') != 'process',
                },
            })

    @classmethod
    def write(cls, forecasts, vals):
        if vals.get('planning_date'):
            for forecast in forecasts:
                replanning = cls.search([
                    ('code_line', '=', forecast.code_line),
                    ('planning_date', '>', forecast.planning_date),
                    ('state', '=', 'open')
                ])
                delta_days = vals.get('planning_date') - forecast.planning_date
                for replan_forecast in replanning:
                    super(Schedule, cls).write(replan_forecast.id, {
                            'planning_date': (replan_forecast.planning_date + delta_days)
                    })
        else:
            super(Schedule, cls).write(forecasts, vals)

    @staticmethod
    def default_state():
        return 'open'

    @fields.depends('line', 'equipment', 'code', 'description',
            'last_date')
    def on_change_line(self):
        if self.line:
            self.equipment = self.line.planning.equipment.id
            self.code = self.line.code.id
            self.description = self.line.description
            self.last_date = self.line.last_maintenance

    @classmethod
    def create_request_service(cls, schedule):
        Request = Pool().get('maintenance.request_service')

        values = {
            'company': Transaction().context.get('company'),
            'request_date': datetime.combine(schedule.planning_date, time()),
            'equipment': schedule.equipment.id,
            'code': schedule.code.id,
            'description': schedule.code.name + " - " + schedule.description,
            'type_activity': schedule.line.planning.type_activity.id,
            'priority': 'low',
            'state': 'open',
            'origin': str(schedule),
        }
        if schedule.plan_period:
            values['period'] = schedule.plan_period.rec_name
        if schedule.equipment.location:
            values['location'] = schedule.equipment.location.id

        request, = Request.create([values])
        Request.set_number([request])

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, schedules):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, schedules):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, schedules):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('process')
    def process(cls, schedules):
        for record in schedules:
            cls.create_request_service(record)

    @classmethod
    def auto_schedule(cls):
        # This methods is used by CRON for create automatic
        # maintenances on scheduled
        Period = Pool().get('maintenance.period')
        Planning = Pool().get('maintenance.planning')
        plannings = Planning.search([])
        today = date.today()
        periods = Period.search([
            ('state', '=', 'open'),
            ('start_date', '<=', today),
            ('end_date', '>=', today),
        ])
        if not periods:
            return

        targets_schedule = {}
        for planning in plannings:
            for line in planning.lines:
                active_schedules = cls.search([
                    ('line', '=', line.id),
                    ('state', 'in', ('open', 'process')),
                ])
                if active_schedules or not line.last_maintenance \
                    or line.id in targets_schedule.keys():
                    continue
                for fline in line.frecuencies_lines:
                    add_schedule = False
                    udm = fline.frecuency_kind.uom_frecuency
                    if fline.frecuency_kind.source_data == 'clock':
                        delta = (today - line.last_maintenance).days * 24
                        add_schedule = cls.analize_time(delta, fline.value, udm)
                    elif fline.frecuency_kind.source_data == 'input_reading':
                        add_schedule = cls.analize_readings(fline, line)

                    if not add_schedule:
                        continue
                    targets_schedule[line.id] = {
                        'plan_period': periods[0].id,
                        'planning': planning.id,
                        'equipment': planning.equipment.id,
                        'description': line.description,
                        'line': line.id,
                        'cause': fline.frecuency_kind.name,
                        'code': line.code.id,
                        'last_date': line.last_maintenance,
                        'work_forecast_hours': line.work_forecast_hours,
                        'planning_date': today + timedelta(line.days_limit_schedule or 7),
                        'state': 'open',
                    }
                    break
        cls.create(targets_schedule.values())

    @classmethod
    def analize_readings(cls, frecuency_line, line):
        return False

    @classmethod
    def analize_time(cls, delta, value, udm):
        res = False
        # Convert all values to hour for normalize compute
        if udm.id == Id('product', 'uom_day').pyson():
            value = value * 24
        elif udm.id == Id('product', 'uom_hour').pyson():
            pass
        else:
            return False
        if delta > value:
            res = True
        return res


class CreateScheduling(Wizard):
    'Create Scheduling'
    __name__ = 'maintenance.create_scheduling'
    start_state = 'create_scheduling'
    create_scheduling = StateTransition()

    def transition_create_scheduling(self):
        Schedule = Pool().get('maintenance.schedule')
        Schedule.auto_schedule()
        return 'end'



class ScheduleForecastStart(ModelView):
    'Schedule Forecast Start'
    __name__ = 'maintenance.print_schedule_forecast.start'
    date = fields.Date('Start Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ScheduleForecast(Wizard):
    'Schedule Forecast'
    __name__ = 'maintenance.print_schedule_forecast'
    start = StateView('maintenance.print_schedule_forecast.start',
            'maintenance.print_schedule_forecast_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'print_', 'tryton-print', default=True),
                ])
    print_ = StateAction('maintenance.report_schedule_forecast')

    def do_print_(self, action):
        company = self.start.company
        data = {
                'date': self.start.date,
                'company': company.id,
                }
        return action, data

    def transition_print_(self):
        return 'end'


class ScheduleForecastReport(Report):
    __name__ = 'maintenance.schedule_forecast.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ScheduleForecastReport, cls).get_context(records, data)
        MAX_DAYS = 30
        pool = Pool()
        Company = pool.get('company.company')
        Schedule = pool.get('maintenance.schedule')

        alldays = {}
        alldays_convert = {}
        for nd in range(MAX_DAYS):
            day_n = 'day' + str((nd + 1))
            tdate = data['date'] + timedelta(nd)
            data[day_n] = tdate
            alldays[day_n] = None
            alldays_convert[tdate] = day_n

        date_limit = data['date'] + timedelta(MAX_DAYS)
        schedules = Schedule.search([
            ('planning_date', '>=', data['date']),
            ('planning_date', '<=', date_limit),
            ('state', '!=', 'cancel'),
        ])

        records = {}
        total_executed = []
        total_no_executed = []
        rate_fullfilment = 0
        sum_dates_deviation = 0.0
        for sch in schedules:
            if sch.state in ['open', 'process']:
                total_no_executed.append(1)
            else:
                total_executed.append(1)
            all_days_copy = alldays.copy()

            if sch.exec_date:
                exec_date = sch.exec_date
            else:
                exec_date = date.today()

            sum_dates_deviation += (sch.planning_date - exec_date).days
            dayn = alldays_convert[sch.planning_date]
            all_days_copy[dayn] = sch.state_string[:1].upper()
            records[sch.id] = {
                    'equipment': sch.equipment.rec_name,
                    'code': sch.code.name,
                    'code_parent': sch.code.parent.name if sch.code.parent else None,
            }
            records[sch.id].update(all_days_copy)
        total_schedule = len(schedules)
        rate_fullfilment = 0
        if total_schedule > 0:
            dates_deviation_avg = sum_dates_deviation / total_schedule
            rate_fullfilment = (len(total_executed) / total_schedule) * 100
        else:
            dates_deviation_avg = None
        report_context['records'] = records.values()
        report_context['total_schedule'] = total_schedule
        report_context['total_executed'] = sum(total_executed)
        report_context['total_no_executed'] = sum(total_no_executed)
        report_context['rate_fullfilment'] = rate_fullfilment
        report_context['dates_deviation_avg'] = dates_deviation_avg
        report_context['date'] = data['date']
        report_context['company'] = Company(data['company']).party.name

        return report_context
