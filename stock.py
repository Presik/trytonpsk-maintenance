# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import Pool, PoolMeta


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'
    origin = fields.Reference('Origin', selection='get_origin', select=True)

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        models = [cls.__name__, 'maintenance.request_service']
        return models

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
                ('model', 'in', models),
            ])
        return [(None, '')] + [(m.model, m.name) for m in models]
